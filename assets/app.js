/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
const $ = require('jquery');
global.$ = global.jQuery = $;
// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';
import 'datatables.net-bs5/js/dataTables.bootstrap5';
import 'datatables.net-bs5/js/dataTables.bootstrap5';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';
// start the Stimulus application
import './bootstrap';

