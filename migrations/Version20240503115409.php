<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240503115409 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE proprietaire_car (proprietaire_id INT NOT NULL, car_id INT NOT NULL, INDEX IDX_8A96E38E76C50E4A (proprietaire_id), INDEX IDX_8A96E38EC3C6F69F (car_id), PRIMARY KEY(proprietaire_id, car_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE proprietaire_car ADD CONSTRAINT FK_8A96E38E76C50E4A FOREIGN KEY (proprietaire_id) REFERENCES proprietaire (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE proprietaire_car ADD CONSTRAINT FK_8A96E38EC3C6F69F FOREIGN KEY (car_id) REFERENCES car (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE car DROP FOREIGN KEY FK_773DE69D710ED0A5');
        $this->addSql('DROP INDEX IDX_773DE69D710ED0A5 ON car');
        $this->addSql('ALTER TABLE car DROP proprietaires_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE proprietaire_car');
        $this->addSql('ALTER TABLE car ADD proprietaires_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D710ED0A5 FOREIGN KEY (proprietaires_id) REFERENCES proprietaire (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_773DE69D710ED0A5 ON car (proprietaires_id)');
    }
}
