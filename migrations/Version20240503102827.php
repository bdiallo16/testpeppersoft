<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240503102827 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE car (id INT AUTO_INCREMENT NOT NULL, proprietaires_id INT DEFAULT NULL, numero_immatriculation VARCHAR(255) DEFAULT NULL, immatriculation_date DATETIME NOT NULL, INDEX IDX_773DE69D710ED0A5 (proprietaires_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE caracteristiques_vehicules (id INT AUTO_INCREMENT NOT NULL, car_id INT DEFAULT NULL, nombre_portes INT NOT NULL, energie VARCHAR(255) NOT NULL, boite_vitesse VARCHAR(255) NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_D513735DC3C6F69F (car_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE proprietaire (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE car ADD CONSTRAINT FK_773DE69D710ED0A5 FOREIGN KEY (proprietaires_id) REFERENCES proprietaire (id)');
        $this->addSql('ALTER TABLE caracteristiques_vehicules ADD CONSTRAINT FK_D513735DC3C6F69F FOREIGN KEY (car_id) REFERENCES car (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE caracteristiques_vehicules DROP FOREIGN KEY FK_D513735DC3C6F69F');
        $this->addSql('ALTER TABLE car DROP FOREIGN KEY FK_773DE69D710ED0A5');
        $this->addSql('DROP TABLE car');
        $this->addSql('DROP TABLE caracteristiques_vehicules');
        $this->addSql('DROP TABLE proprietaire');
    }
}
