<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Car;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class CarFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');
        for($nbCar = 1; $nbCar <= 5; $nbCar++){

          $proprietaires = $this->getReference('proprietaires_'.$faker->numberBetween(0, 4));
           
            $cars = new Car();
            $cars->setProprietaires($proprietaires);
                   
            $cars->setNumeroImmatriculation($faker->randomNumber(5, true));
            $cars->setImmatriculationDate($faker->dateTime());
            $manager->persist($cars);

            // Enregistre le dept dans une référence
            $this->addReference('cars_'.$nbCar, $cars);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ProprietaireFixtures::class
        ];
    }
    
    }

