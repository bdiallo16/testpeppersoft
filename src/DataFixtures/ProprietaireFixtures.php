<?php

namespace App\DataFixtures;

use App\Entity\Proprietaire;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
class ProprietaireFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($nbUsers = 1; $nbUsers <= 5; $nbUsers++) {
            $proprietaire = new Proprietaire();
            $proprietaire->setNom($faker->firstName);
            $proprietaire->setPrenom($faker->lastName);
            $manager->persist($proprietaire);

            // Enregistre l'utilisateur dans une référence
            $this->addReference('proprietaires_'.$nbUsers, $proprietaire);
        }
        $manager->flush();
    }
}
