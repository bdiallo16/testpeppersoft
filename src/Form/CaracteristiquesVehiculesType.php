<?php

namespace App\Form;

use App\Entity\Car;
use Symfony\Component\Form\AbstractType;
use App\Entity\CaracteristiquesVehicules;
use App\Repository\CarRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaracteristiquesVehiculesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('nombrePortes'     
            )
            ->add('energie'  )
            ->add('boiteVitesse')
         ->add('country', EntityType::class, [
            'class' => Car::class,
            'choice_label' => 'name',
            'placeholder' => 'Choose a country',
            'query_builder' => function (CarRepository $er) {
            return $er->createQueryBuilder('c')
            ->orderBy('c.immatriculationDate', 'ASC');
    },
        ]);

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CaracteristiquesVehicules::class,
        ]);
    }
}
