<?php

namespace App\Form;

use DateTime;
use App\Entity\Car;
use App\Entity\Modele;
use App\Entity\Proprietaire;
use Doctrine\DBAL\Types\DateTimeType;
use Symfony\Component\Form\AbstractType;
use App\Entity\CaracteristiquesVehicules;
use App\Repository\CaracteristiquesVehiculesRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class CarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('numeroImmatriculation')
            ->add(
                'immatriculationDate',
                DateType::class,
                [
                    'widget' => 'choice',
                    'label'=>'Date Immatriculation'
                ]

                );
            // ->add('caracteristiquesVehicules', EntityType::class, [
            //     'class' => CaracteristiquesVehicules::class,
            //     'choice_label' => 'name',
            //     'placeholder' => 'Choose a country',
            //     'query_builder' => function (CaracteristiquesVehiculesRepository $er) {
            //     return $er->createQueryBuilder('c')
            //     ->orderBy('c.id', 'ASC');
            // },
            // ]);

           
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
