<?php

namespace App\Entity;

use App\Repository\CaracteristiquesVehiculesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CaracteristiquesVehiculesRepository::class)
 */
class CaracteristiquesVehicules
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $nombrePortes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $energie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $boiteVitesse;

    /**
     * @ORM\ManyToOne(targetEntity=Car::class, inversedBy="caracteristiquesVehicules",cascade={"persist", "remove"})
     */
    private $car;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modele;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombrePortes(): ?int
    {
        return $this->nombrePortes;
    }

    public function setNombrePortes(int $nombrePortes): self
    {
        $this->nombrePortes = $nombrePortes;

        return $this;
    }

    public function getEnergie(): ?string
    {
        return $this->energie;
    }

    public function setEnergie(string $energie): self
    {
        $this->energie = $energie;

        return $this;
    }

    public function getBoiteVitesse(): ?string
    {
        return $this->boiteVitesse;
    }

    public function setBoiteVitesse(string $boiteVitesse): self
    {
        $this->boiteVitesse = $boiteVitesse;

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    

   

    /**
     * Get the value of modele
     */ 
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * Set the value of modele
     *
     * @return  self
     */ 
    public function setModele($modele)
    {
        $this->modele = $modele;

        return $this;
    }
}
