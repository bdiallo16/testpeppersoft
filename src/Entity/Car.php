<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarRepository::class)
 */
class Car
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $numeroImmatriculation;

    /**
     * @ORM\Column(type="datetime")
     */
    private $immatriculationDate;




    /**
     * @ORM\OneToMany(targetEntity=CaracteristiquesVehicules::class, mappedBy="car", orphanRemoval=true,cascade={"persist", "remove"})
     */
    private $caracteristiquesVehicules;

    /**
     * @ORM\ManyToMany(targetEntity=Proprietaire::class, mappedBy="cars")
     */
    private $proprietaires;


    public function __construct()
    {
        $this->caracteristiquesVehicules = new ArrayCollection();
        $this->immatriculationDate = new \DateTimeImmutable();
        $this->proprietaires = new ArrayCollection();
    }

    

   

    public function getId(): ?int
    {
        return $this->id;
    }

    


    public function getNumeroImmatriculation(): ?string
    {
        return $this->numeroImmatriculation;
    }

    public function setNumeroImmatriculation(?string $numeroImmatriculation): self
    {
        $this->numeroImmatriculation = $numeroImmatriculation;

        return $this;
    }

    public function getImmatriculationDate(): ?\DateTimeInterface
    {
        return $this->immatriculationDate;
    }

    public function setImmatriculationDate(\DateTimeInterface $immatriculationDate): self
    {
        $this->immatriculationDate = $immatriculationDate;

        return $this;
    }



    /**
     * @return Collection<int, CaracteristiquesVehicules>
     */
    public function getCaracteristiquesVehicules(): Collection
    {
        return $this->caracteristiquesVehicules;
    }

    public function addCaracteristiquesVehicule(CaracteristiquesVehicules $caracteristiquesVehicule): self
    {
        if (!$this->caracteristiquesVehicules->contains($caracteristiquesVehicule)) {
            $this->caracteristiquesVehicules[] = $caracteristiquesVehicule;
            $caracteristiquesVehicule->setCar($this);
        }

        return $this;
    }

    public function removeCaracteristiquesVehicule(CaracteristiquesVehicules $caracteristiquesVehicule): self
    {
        if ($this->caracteristiquesVehicules->removeElement($caracteristiquesVehicule)) {
            // set the owning side to null (unless already changed)
            if ($caracteristiquesVehicule->getCar() === $this) {
                $caracteristiquesVehicule->setCar(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Proprietaire>
     */
    public function getProprietaires(): Collection
    {
        return $this->proprietaires;
    }

    public function addProprietaire(Proprietaire $proprietaire): self
    {
        if (!$this->proprietaires->contains($proprietaire)) {
            $this->proprietaires[] = $proprietaire;
            $proprietaire->addCar($this);
        }

        return $this;
    }

    public function removeProprietaire(Proprietaire $proprietaire): self
    {
        if ($this->proprietaires->removeElement($proprietaire)) {
            $proprietaire->removeCar($this);
        }

        return $this;
    }

    

    
}
