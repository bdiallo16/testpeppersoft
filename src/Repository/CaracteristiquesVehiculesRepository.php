<?php

namespace App\Repository;

use App\Entity\CaracteristiquesVehicules;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<CaracteristiquesVehicules>
 *
 * @method CaracteristiquesVehicules|null find($id, $lockMode = null, $lockVersion = null)
 * @method CaracteristiquesVehicules|null findOneBy(array $criteria, array $orderBy = null)
 * @method CaracteristiquesVehicules[]    findAll()
 * @method CaracteristiquesVehicules[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CaracteristiquesVehiculesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CaracteristiquesVehicules::class);
    }

    public function getVehicules()
    {
        return $this->createQueryBuilder('cv')
            ->innerJoin('cv.car', 'c')


            ->getQuery()
            ->getResult()
            ;
    }

    public function add(CaracteristiquesVehicules $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(CaracteristiquesVehicules $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return CaracteristiquesVehicules[] Returns an array of CaracteristiquesVehicules objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?CaracteristiquesVehicules
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
