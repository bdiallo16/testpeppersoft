<?php

namespace App\Controller;

use App\Repository\CarRepository;
use Symfony\UX\Chartjs\Model\Chart;
use App\Repository\ProprietaireRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DataController extends AbstractController
{
    /**
     * @Route("/data", name="app_data")
     */
    public function index(CarRepository $carRepository, ChartBuilderInterface $chartBuilder): Response
    {
        $nbVehicules = $carRepository->findAll();
        
        $labels = [];
        $data = [];

        foreach ($nbVehicules as $nbVehicule) {
            $labels[] = $nbVehicule->getImmatriculationDate()->format('d/m/Y');
            $data[] = $nbVehicule->getNumeroImmatriculation();
        }

        $chart = $chartBuilder->createChart(Chart::TYPE_LINE);
        $chart->setData([
            'labels' => $labels,
            'datasets' => [
                [
                    'label' => '',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => $data,
                ],
            ],
        ]);

        $chart->setOptions([]);


        return $this->render('data/index.html.twig', [
            
            'chart' => $chart,
        ]);
    }


}
