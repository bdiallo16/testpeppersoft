<?php

namespace App\Controller;

use App\Entity\CaracteristiquesVehicules;
use App\Form\CaracteristiquesVehicules1Type;
use App\Repository\CaracteristiquesVehiculesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/caracteristiques/vehicules")
 */
class CaracteristiquesVehiculesController extends AbstractController
{
    /**
     * @Route("/", name="app_caracteristiques_vehicules_index", methods={"GET"})
     */
    public function index(CaracteristiquesVehiculesRepository $caracteristiquesVehiculesRepository): Response
    {
        return $this->render('caracteristiques_vehicules/index.html.twig', [
            'caracteristiques_vehicules' => $caracteristiquesVehiculesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_caracteristiques_vehicules_new", methods={"GET", "POST"})
     */
    public function new(Request $request, CaracteristiquesVehiculesRepository $caracteristiquesVehiculesRepository): Response
    {
        $caracteristiquesVehicule = new CaracteristiquesVehicules();
        $form = $this->createForm(CaracteristiquesVehicules1Type::class, $caracteristiquesVehicule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $caracteristiquesVehiculesRepository->add($caracteristiquesVehicule, true);

            return $this->redirectToRoute('app_caracteristiques_vehicules_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('caracteristiques_vehicules/new.html.twig', [
            'caracteristiques_vehicule' => $caracteristiquesVehicule,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_caracteristiques_vehicules_show", methods={"GET"})
     */
    public function show(CaracteristiquesVehicules $caracteristiquesVehicule): Response
    {
        return $this->render('caracteristiques_vehicules/show.html.twig', [
            'caracteristiques_vehicule' => $caracteristiquesVehicule,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_caracteristiques_vehicules_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, CaracteristiquesVehicules $caracteristiquesVehicule, CaracteristiquesVehiculesRepository $caracteristiquesVehiculesRepository): Response
    {
        $form = $this->createForm(CaracteristiquesVehicules1Type::class, $caracteristiquesVehicule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $caracteristiquesVehiculesRepository->add($caracteristiquesVehicule, true);

            return $this->redirectToRoute('app_caracteristiques_vehicules_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('caracteristiques_vehicules/edit.html.twig', [
            'caracteristiques_vehicule' => $caracteristiquesVehicule,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_caracteristiques_vehicules_delete", methods={"POST"})
     */
    public function delete(Request $request, CaracteristiquesVehicules $caracteristiquesVehicule, CaracteristiquesVehiculesRepository $caracteristiquesVehiculesRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$caracteristiquesVehicule->getId(), $request->request->get('_token'))) {
            $caracteristiquesVehiculesRepository->remove($caracteristiquesVehicule, true);
        }

        return $this->redirectToRoute('app_caracteristiques_vehicules_index', [], Response::HTTP_SEE_OTHER);
    }
}
